﻿using System;
using System.Collections.Generic;
using System.Text;
using ZLibrary;
using ZLibrary.Constants;
using ZLibrary.Managers.Objects;
using ZLibrary.Tools;

namespace Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            ZCore.Boot();

            Console.Read();
        }
    }
}
