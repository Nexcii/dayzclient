using System;

namespace XNAMapView
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (MapCore game = new MapCore())
            {
                game.Run();
            }
        }
    }
#endif
}

