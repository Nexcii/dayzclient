﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace XNAMapView.Tools
{
    public class Tile
    {
        private int x, y;
        private Texture2D texture;

        public Tile(int _x, int _y, Texture2D _texture)
        {
            x = _x;
            y = _y;
            texture = _texture;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Vector2(x * 256, y * 256), Color.White);
        }
    }
}
