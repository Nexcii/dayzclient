﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace XNAMapView.Tools
{
    public class Camera
    {
        public Vector2 Position = Vector2.Zero;
        public float Zoom = 1;

        public Camera()
        {
        }

        public void Update(GameTime gameTime)
        {
            // [Basic Movement]
            if (Keyboard.GetState().IsKeyDown(Keys.Down)) Position.Y -= 8;
            if (Keyboard.GetState().IsKeyDown(Keys.Right)) Position.X -= 8;
            if (Keyboard.GetState().IsKeyDown(Keys.Up)) Position.Y += 8;
            if (Keyboard.GetState().IsKeyDown(Keys.Left)) Position.X += 8;

            if (Keyboard.GetState().IsKeyDown(Keys.OemPlus)) SetZoom(Zoom + 0.01f);
            if (Keyboard.GetState().IsKeyDown(Keys.OemMinus)) SetZoom(Zoom - 0.01f);
        }

        public void SetZoom(float zoom)
        {
            Zoom = MathHelper.Clamp(zoom, 0.001f, 3f);
        }

        public Vector2 WorldToScreen(Vector2 worldPosition)
        {
            return Vector2.Transform(worldPosition, Matrix);
        }

        public Matrix Matrix
        {
            get
            {
                return Matrix.CreateTranslation(new Vector3(Position.X, Position.Y, 0)) * Matrix.CreateScale(Zoom) * Matrix.CreateTranslation(new Vector3(1280 / 2, 720 / 2, 0));
            }
        }
    }
}
