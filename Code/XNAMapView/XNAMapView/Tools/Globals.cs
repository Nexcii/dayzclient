﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XNAMapView.Tools
{
    public class Globals
    {
        public const int DefaultScreenWidth = 1280;
        public const int DefaultScreenHeight = 720;

        public static string ContentDirectory = "Content";

        public static Color ScreenColour = Color.Salmon;
    }
}
