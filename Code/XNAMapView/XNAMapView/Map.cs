﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using XNAMapView.Tools;
using ZLibrary.Tools;
using ZLibrary.Managers.Objects;
using ZLibrary;
using ZLibrary.Constants;
using ZLibrary.Managers;

namespace XNAMapView
{
    public enum Icons
    {
        // [Buildings]
        DeerStand,
        FuelPump,
        Hospital,
        SuperMarket,
        WaterPump,

        // [Other]
        BlueSmall,
        BlueBig,
        BlueSquare,

        GreenSmall,
        GreenBig,
        GreenSquare,

        RedSmall,
        RedBig,
        RedSquare,

        YellowSmall,
        YellowBig,
        YellowSquare,

        // [Vehicles]
        ATV,
        BigTruck,
        Bike,
        Boat,
        Bus,
        Car,
        Helicopter,
        Motorcycle,
        Tractor,
        Truck,
        UAZ
    }

    public class Map
    {
        public const int XTILE_COUNT = 16;
        public const int YTILE_COUNT = 14;

        private SpriteBatch spriteBatch;

        private SpriteFont basicFont;

        private Dictionary<Icons, Texture2D> iconLibrary;
        private Tile[,] Tiles;

        private Texture2D linetexture;

        public Map()
        {
            spriteBatch = new SpriteBatch(MapCore.Graphics);
        }

        public void LoadContent()
        {
            LoadTiles();
            LoadIcons();
            linetexture = MapCore.ConentManager.Load<Texture2D>("Images//linetexture");
            basicFont = MapCore.ConentManager.Load<SpriteFont>("Fonts//Verdana");
        }

        private void LoadTiles()
        {
            Tiles = new Tile[XTILE_COUNT, YTILE_COUNT];

            for (int x = 0; x < Tiles.GetLength(0); x++)
            {
                for (int y = 0; y < Tiles.GetLength(1); y++)
                {
                    Tiles[x, y] = new Tile(x, y, MapCore.ConentManager.Load<Texture2D>("Tiles/" + x + "_" + y));
                }
            }
        }

        private void LoadIcons()
        {
            iconLibrary = new Dictionary<Icons, Texture2D>();

            // [Building]
            AddIcon(Icons.DeerStand, "Icons//Buildings//DeerStand");
            AddIcon(Icons.FuelPump, "Icons//Buildings//FuelPump");
            AddIcon(Icons.Hospital, "Icons//Buildings//Hospital");
            AddIcon(Icons.SuperMarket, "Icons//Buildings//SuperMarket");
            AddIcon(Icons.WaterPump, "Icons//Buildings//WaterPump");

            // [Other]
            AddIcon(Icons.BlueSmall, "Icons//Other//Blue_Small");
            AddIcon(Icons.BlueBig, "Icons//Other//Blue_Big");
            AddIcon(Icons.BlueSquare, "Icons//Other//Blue_Square");

            AddIcon(Icons.GreenSmall, "Icons//Other/Green_Small");
            AddIcon(Icons.GreenBig, "Icons//Other//Green_Big");
            AddIcon(Icons.GreenSquare, "Icons//Other//Green_Square");

            AddIcon(Icons.RedSmall, "Icons//Other/Red_Small");
            AddIcon(Icons.RedBig, "Icons//Other//Red_Big");
            AddIcon(Icons.RedSquare, "Icons//Other//Red_Square");

            AddIcon(Icons.YellowSmall, "Icons//Other//Yellow_Small");
            AddIcon(Icons.YellowBig, "Icons//Other//Yellow_Big");
            AddIcon(Icons.YellowSquare, "Icons//Other//Yellow_Square");

            // [Vehicles]
            AddIcon(Icons.ATV, "Icons//Vehicle//ATV");
            AddIcon(Icons.BigTruck, "Icons//Vehicle//BigTruck");
            AddIcon(Icons.Bike, "Icons//Vehicle//Bike");
            AddIcon(Icons.Boat, "Icons//Vehicle//Boat");
            AddIcon(Icons.Bus, "Icons//Vehicle//Bus");
            AddIcon(Icons.Car, "Icons//Vehicle//Car");
            AddIcon(Icons.Helicopter, "Icons//Vehicle//Helicopter");
            AddIcon(Icons.Motorcycle, "Icons//Vehicle//Motorcycle");
            AddIcon(Icons.Tractor, "Icons//Vehicle//Tractor");
            AddIcon(Icons.Truck, "Icons//Vehicle//Truck");
            AddIcon(Icons.UAZ, "Icons//Vehicle//UAZ");
        }

        public void Draw()
        {
            DrawMap();

            try
            {
                foreach (Entity entity in ZCore.EntityManager.Entities)
                {


                    if (entity.Type == EntityType.Vehicle)
                    {
                        DrawIconWithText(iconFromClassName(entity.ClassName), entity.ClassName, armaPositionToScreen(entity.Position));
                    }

                    if (InfoManager.Names.ContainsKey(entity.ObjectType))
                    {
                        DrawIconWithText(iconFromClassName(entity.ClassName), entity.PlayerName, armaPositionToScreen(entity.Position));
                    }
                    else
                    {
                        // Probably dead?
                        //DrawIconWithText(iconFromClassName(entity.ClassName), "Unknown", armaPositionToScreen(entity.Position));
                    }

                    if (entity.Type == EntityType.Other)
                    {
                        DrawIconWithText(iconFromClassName(entity.ClassName), entity.ClassName, armaPositionToScreen(entity.Position));
                    }
                }




            }
            catch (Exception e)
            {
            }
        }

        public void DrawMap()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp, DepthStencilState.Default, RasterizerState.CullNone, null, MapCore.Camera.Matrix);

            for (int x = 0; x < Tiles.GetLength(0); x++)
            {
                for (int y = 0; y < Tiles.GetLength(1); y++)
                {
                    Tiles[x, y].Draw(spriteBatch);
                }
            }

            spriteBatch.End();
        }

        public void DrawIconWithText(Icons icon, String text, Vector2 position)
        {
            Vector2 centerOffset = new Vector2((int)(iconLibrary[icon].Width / 2), (int)(iconLibrary[icon].Height / 2));
            Vector2 textSize = basicFont.MeasureString(text);

            spriteBatch.Begin();
            spriteBatch.Draw(iconLibrary[icon], position - centerOffset, Color.White);

            // Outline
            spriteBatch.DrawString(basicFont, text, position + new Vector2(-(int)(textSize.X / 2), 09), Color.Black);
            spriteBatch.DrawString(basicFont, text, position + new Vector2(-(int)(textSize.X / 2) - 1, 10), Color.Black);
            spriteBatch.DrawString(basicFont, text, position + new Vector2(-(int)(textSize.X / 2), 11), Color.Black);
            spriteBatch.DrawString(basicFont, text, position + new Vector2(-(int)(textSize.X / 2) + 1, 10), Color.Black);

            spriteBatch.DrawString(basicFont, text, position + new Vector2(-(int)(textSize.X / 2), 10), Color.White);

            spriteBatch.End();
        }

        public void DrawLine(Texture2D texture, Vector2 start, Vector2 end)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(texture, start, null, Color.Red,
                             (float)Math.Atan2(end.Y - start.Y, end.X - start.X),
                             new Vector2(0f, (float)texture.Height / 2),
                             new Vector2(Vector2.Distance(start, end), 1f),
                             SpriteEffects.None, 0f);
            spriteBatch.End();
        }

        public void DrawIcon(Icons icon, Vector2 position)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(iconLibrary[icon], position, Color.White);
            spriteBatch.End();
        }

        // -------------------------------------------------------------------------------------
        // -- Helper Function 
        // -------------------------------------------------------------------------------------

        private void AddIcon(Icons iconEnum, String iconURL)
        {
            Console.WriteLine("[Content] Loading " + iconURL);
            iconLibrary.Add(iconEnum, MapCore.ConentManager.Load<Texture2D>(iconURL));
        }

        private Vector2 armaPositionToScreen(Vector3 armaPosition)
        {
            Vector2 position = MapCore.Camera.WorldToScreen(armaPositionToMap(armaPosition));
            return new Vector2((int)position.X, (int)position.Y);
        }

        private Vector2 armaPositionToScreen(Vector2 armaPosition)
        {
            Vector2 position = MapCore.Camera.WorldToScreen(armaPositionToMap(new Vector3(armaPosition.X, armaPosition.Y, 0)));
            return new Vector2((int)position.X, (int)position.Y);
        }

        private Vector2 armaPositionToMap(Vector3 world)
        {
            float xRawPosition = world.X - 336;
            float yRawPosition = (13405 - world.Y);
            float xRatio = (float)(xRawPosition / (14757 - 336));
            float yRatio = (float)(yRawPosition / (13460 - 1442));

            float mapWidth = ((Map.XTILE_COUNT) * 255);
            float mapHeight = ((Map.YTILE_COUNT - 1) * 255) + 82;

            float endX = mapWidth * xRatio;
            float endY = mapHeight * yRatio;

            return new Vector2(endX, endY);
        }

        private Icons iconFromClassName(string className)
        {
            EntityType type = Utils.GetTypeFromClass(className);

            if (type == EntityType.Player) return Icons.GreenBig;

            if (type == EntityType.Vehicle)
            {
                switch (className)
                {
                    case "UH1H_DZ":
                        return Icons.Helicopter;

                    case "Tractor":
                        return Icons.Tractor;

                    case "car_hatchback":
                    case "Lada2":
                    case "Volha_2_TK_CIV_EP1":
                        return Icons.Car;

                    case "TT650_Ins":
                    case "TT650_Civ":
                        return Icons.Motorcycle;

                    case "Old_bike_TK_CIV_EP1":
                    case "Old_bike_TK_INS_EP1":
                        return Icons.Bike;

                    case "Ikarus":
                    case "S1203_TK_CIV_EP1":
                        return Icons.Bus;

                    case "V3S_Civ":
                    case "Ural_TK_CIV_EP1":
                        return Icons.BigTruck;

                    case "UAZ_CDF":
                        return Icons.UAZ;

                    case "hilux_civil_3_open":
                    case "hilux1_civil_3_open":
                        return Icons.Truck;

                    case "Smallboat_1":
                    case "smallboat_2":
                    case "PBX":
                        return Icons.Boat;

                    case "ATV_CZ_EP1":
                    case "ATV_US_EP1":
                        return Icons.ATV;

                    default:
                        return Icons.YellowSquare;
                }
            }

            if (className.ToLower().Contains("tent")) return Icons.SuperMarket;
            if (className.ToLower().Contains("uh1wreck")) return Icons.Hospital;

            return Icons.WaterPump;
        }

        public static string[] VEHICLE_MODELS = new string[21] {
            "UH1H_DZ",
            "car_hatchback",
            "Tractor",
            "TT650_Ins",
            "TT650_Civ",
            "Old_bike_TK_CIV_EP1",
            "Old_bike_TK_INS_EP1",
            "Ikarus",
            "V3S_Civ",
            "Ural_TK_CIV_EP1",
            "Lada2",
            "UAZ_CDF",
            "Volha_2_TK_CIV_EP1",
            "hilux_civil_3_open",
            "hilux1_civil_3_open",
            "S1203_TK_CIV_EP1",
            "Smallboat_1",
            "smallboat_2",
            "PBX",
            "ATV_CZ_EP1",
            "ATV_US_EP1"
        };

    }
}
