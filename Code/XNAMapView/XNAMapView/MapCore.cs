using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XNAMapView.Tools;
using ZLibrary;
using ZLibrary.Constants;
using ZLibrary.Managers.Objects;
using ZLibrary.Tools;
using XNAMapView.Input;
using System.Threading;

namespace XNAMapView
{
    public class MapCore : Microsoft.Xna.Framework.Game
    {
        public static GraphicsDeviceManager GraphicsManager;
        public static GraphicsDevice Graphics;
        public static ContentManager ConentManager;
        public static Game Game;

        public static Map Map;
        public static Camera Camera;

        public MapCore()
        {
            GraphicsManager = new GraphicsDeviceManager(this);

            ConentManager = Content;
            Game = this;

            ChangeResolution(Globals.DefaultScreenWidth, Globals.DefaultScreenHeight);
        }

        protected override void Initialize()
        {
            Graphics = GraphicsManager.GraphicsDevice;

            IsMouseVisible = true;
            ConentManager.RootDirectory = Globals.ContentDirectory;

            Map = new Map();
            Camera = new Camera();

            ZCore.Boot();

            base.Initialize(); 
        }

        protected override void LoadContent()
        {
            Map.LoadContent();
        }

        protected override void UnloadContent()
        {
            // Fuck dat shit
        }

        protected override void Update(GameTime gameTime)
        {
            MouseInput.UpdateStart();
            KeyboardInput.UpdateStart();

            Camera.Update(gameTime);

            if (MouseInput.isLeftDown())
            {
                Console.WriteLine("Hello");
            }

            if (KeyboardInput.isKeyDown(Keys.A))
            {
                Console.WriteLine("Arrrrgrghh!");
            }

            UpdateEntities(gameTime);
           

            base.Update(gameTime);

            MouseInput.UpdateEnd();
            KeyboardInput.UpdateEnd();
        }

        private const int TICK_TIME = 500;
        private float elpasedTime = 0;

        private Thread entityThread;

        private void UpdateEntities(GameTime gameTime)
        {
            elpasedTime += gameTime.ElapsedGameTime.Milliseconds;

            if (elpasedTime > TICK_TIME)
            {
                entityThread = new Thread(new ThreadStart(UpdateEntityThread));
                entityThread.Start();

                elpasedTime -= TICK_TIME;
            }
        }

        private void UpdateEntityThread()
        {
            //ZCore.Player.Update();

            foreach (Entity entity in ZCore.EntityManager.Entities) entity.Update();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Globals.ScreenColour);

            Map.Draw(); // Draw Map Yo!
           
            base.Draw(gameTime);
        }

        private void ChangeResolution(int Width, int Height)
        {
            GraphicsManager.PreferredBackBufferWidth = Width;
            GraphicsManager.PreferredBackBufferHeight = Height;
            GraphicsManager.ApplyChanges();
        }
    }
}
