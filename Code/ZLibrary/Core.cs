﻿using System;
using System.Collections.Generic;
using System.Text;
using Magic;
using ZLibrary.Constants;
using ZLibrary.Managers;
using System.Threading;
using ZLibrary.Managers.Objects;

namespace ZLibrary
{
    public class ZCore
    {
        private static BlackMagic blackMagic;
        private static uint mainOffsetPtr;
        private static uint mainInfoOffsetPtr;

        private static Player player;
        private static EntityManager entityManager;

        public static void Boot()
        {
            blackMagic = new BlackMagic();
            blackMagic.OpenProcessAndThread(SProcess.GetProcessFromProcessName(ZConstants.APPLICATION));

            mainOffsetPtr = blackMagic.ReadUInt(ZConstants.BASE_96061);
            mainInfoOffsetPtr = BlackMagic.ReadUInt(ZConstants.BASE_INFO_96061);

            InfoManager.Boot();
            entityManager = new EntityManager();
            //player = new Player();

            entityManager.Update();

            
        }

        public static BlackMagic BlackMagic { get { return blackMagic; } }
        public static uint MainOffsetPtr { get { return mainOffsetPtr; } }
        public static uint MainInfoOffsetPtr { get { return mainInfoOffsetPtr; } }

        public static Player Player { get { return player; } }
        public static EntityManager EntityManager { get { return entityManager; } }
    }
}
