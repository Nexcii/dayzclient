﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLibrary.Constants;
using Microsoft.Xna.Framework;

namespace ZLibrary.Tools
{
    public class Utils
    {
        public static float DegreeToRadian(double angle)
        {
            return (float)(Math.PI * angle / 180.0);
        }

        public static Vector2 Vector3ToVector2(Vector3 vector)
        {
            return new Vector2(vector.X, vector.Y);
        }

        public static EntityType GetTypeFromClass(String className)
        {
            if (ZConstants.PLAYER_MODELS.Contains(className)) return EntityType.Player;
            if (ZConstants.VEHICLE_MODELS.Contains(className)) return EntityType.Vehicle;
            if (ZConstants.WANTED_ITEMS.Contains(className)) return EntityType.Other;

            return EntityType.Unknown;
        }

    }
}
