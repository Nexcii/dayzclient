﻿using System;
using System.Collections.Generic;
using System.Text;
using ZLibrary.Managers.Objects;
using ZLibrary.Tools;
using System.IO;
using ZLibrary.Constants;

namespace ZLibrary.Managers
{
    public class EntityManager
    {
        public uint[] tab_offsets = { 0x4, 0xac, 0x154, 0x1fc, 0x54c, 0x5f4, 0x7ec, 0x744 };

        /* Not 100%s
         * 0x4 = Players/Zombies
         * 0xac = Zombies/NPCS
         * 0x154 = Players/Zombies
         * 0x1fc = Player/Zombie
         * 0x54c = Weapon Holders
         * 
         */

        private List<Entity> entitys;

        public EntityManager()
        {
            
        }
 
        public void Update()
        {
            entitys = new List<Entity>();

            for (int i = 0; i < tab_offsets.Length; ++i)
            {
                uint size = ZCore.BlackMagic.ReadUInt(ZCore.MainOffsetPtr + 0x87C + 8 + tab_offsets[i]);

                ReadTable(ZCore.MainOffsetPtr + 0x87C + tab_offsets[i], size);
            }
        }

        private void ReadTable(uint address, uint size)
        {
            for (int i = 0; i < size; ++i)
            {
                uint entity = ZCore.BlackMagic.ReadUInt(ZCore.BlackMagic.ReadUInt(address + 0x4) + (uint)(0x4 * i));
                uint classNamePtr = ZCore.BlackMagic.ReadUInt(ZCore.BlackMagic.ReadUInt(entity + 0x3C) + 0x30);
                String className = ZCore.BlackMagic.ReadASCIIString(classNamePtr + 0x8, 24);

                AddEntity(entity);
            }
        }

        private void AddEntity(uint entityPtr)
        {
            Entity entity = new Entity(entityPtr);

            if (VerifyEntity(entity))
            {
                if (entity.Type != EntityType.Unknown)
                {
                    entitys.Add(entity);

                    if (entity.ObjectType == 1)
                    {
                        string wat = ZCore.BlackMagic.ReadASCIIString(ZCore.BlackMagic.ReadUInt(entity.EntityAddress + 0xa20) + 0x8, 20);
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(entity.ClassName + " = " + entity.PlayerName);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Error " + entity.ClassName); 
            }
        }

        private bool VerifyEntity(Entity entity)
        {
            try
            {
                float x = entity.Position.X;
                float y = entity.Position.Y;
                float z = entity.Position.Z;

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Entity> Entities { get { return entitys; } }
    }



}
