﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLibrary.Tools;
using ZLibrary.Constants;
using Microsoft.Xna.Framework;

namespace ZLibrary.Managers
{
    public class Player
    {
        private uint playerObj;
        private uint playerPositionObj;

        public Player()
        {
            playerObj = ZCore.BlackMagic.ReadUInt(ZCore.MainOffsetPtr + 0x13A4);
            playerPositionObj = ZCore.BlackMagic.ReadUInt(ZCore.BlackMagic.ReadUInt(playerObj + 0x4) + 0x18);
        }

        public Vector3 Position
        {
            get
            {
                float x = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x28);
                float y = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x30);
                float z = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x2C);

                return new Vector3(x, y, z);
            }
        }

        public float Rotation
        {
            get
            {
                float vecDirection1 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x1C);
                float vecDirection2 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x24);

                return (float)(Math.Atan2(vecDirection1, vecDirection2) * (180f / Math.PI));
            }
        }

        public Vector2 Direction
        {
            get
            {
                float vecDirection1 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x1C);
                float vecDirection2 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x24);

                return new Vector2(vecDirection1, vecDirection2);
            }
        }

        public Vector3 FullDirection
        {
            get
            {
                float vecDirection1 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x1C);
                float vecDirection2 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x24);
                float vecDirection3 = ZCore.BlackMagic.ReadFloat(playerPositionObj + 0x2C);

                return new Vector3(vecDirection1, vecDirection2, vecDirection3);
            }
        }

        public void WriteDirVector(Vector2 dir)
        {
            ZCore.BlackMagic.WriteFloat(playerPositionObj + 0x1C, dir.X);
            ZCore.BlackMagic.WriteFloat(playerPositionObj + 0x24, dir.Y);
        }
        

        public float RotationRadian
        {
            get
            {
                return MathHelper.ToRadians(Rotation);
            }
        }

        public Vector2 GetRotationVector()
        {
            float x = Position.X + (float)Math.Sin(RotationRadian) * 1000;
            float y = Position.Y + (float)Math.Cos(RotationRadian) * 1000;
            return new Vector2(x, y);
        }

    }
}