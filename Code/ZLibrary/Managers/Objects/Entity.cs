﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLibrary.Tools;
using ZLibrary.Constants;
using Microsoft.Xna.Framework;

namespace ZLibrary.Managers.Objects
{
    public class Entity
    {
        private uint entityObj;

        public String ClassName;
        public Vector3 Position;
        public float Rotation;
        public Vector2 RotationVector;
        public bool IsAlive;
        public uint ObjectType;

        public uint EntityAddress
        {
            get
            {
                return entityObj;
            }
        }

        public String PlayerName // Hack
        {
            get
            {
                return InfoManager.Names[ObjectType];
            }
        }

        public Entity(uint _entityObj)
        {
            entityObj = _entityObj;

            Update();
        }

        public void Update()
        {
            ClassName = GetClassName();
            Position = GetPosition();
            Rotation = GetRotation();
            RotationVector = GetRotationVector();
            IsAlive = GetIsAlive();
            ObjectType = GetObjectType();
        }

        public uint GetObjectType()
        {
            return ZCore.BlackMagic.ReadUInt(entityObj + 0xac8);
        }

        public String GetClassName()
        {
            uint classNamePtr = ZCore.BlackMagic.ReadUInt(ZCore.BlackMagic.ReadUInt(entityObj + 0x3C) + 0x30);
            return ZCore.BlackMagic.ReadASCIIString(classNamePtr + 0x8, 24);
        }

        public Vector3 GetPosition()
        {
            uint positonData = ZCore.BlackMagic.ReadUInt(entityObj + 0x18);
            
            float x = ZCore.BlackMagic.ReadFloat(positonData + 0x28);
            float y = ZCore.BlackMagic.ReadFloat(positonData + 0x30);
            float z = ZCore.BlackMagic.ReadFloat(positonData + 0x2C);
            
            return new Vector3(x, y, z);
        }

        public float GetRotation()
        {
            uint positonData = ZCore.BlackMagic.ReadUInt(entityObj + 0x18);

            float vecDirection1 = ZCore.BlackMagic.ReadFloat(positonData + 0x1C);
            float vecDirection2 = ZCore.BlackMagic.ReadFloat(positonData + 0x24);

            return (float)(Math.Atan2(vecDirection1, vecDirection2) * (180f / Math.PI));
        }

        public float RotationRadian
        {
            get
            {
                return Utils.DegreeToRadian(Rotation);
            }
        }

        public Vector2 GetRotationVector()
        {
            float x = Position.X + (float)Math.Sin(RotationRadian) * 200;
            float y = Position.Y + (float)Math.Cos(RotationRadian) * 200;
            return new Vector2(x, y);
        }

        public bool GetIsAlive()
        {
            return (ZCore.BlackMagic.ReadByte(entityObj + 0x20C) == 1);
        }

        public EntityType Type
        {
            get
            {
                return Utils.GetTypeFromClass(ClassName);
            }
        }
    }
}
