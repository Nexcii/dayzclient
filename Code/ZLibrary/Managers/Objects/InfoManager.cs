﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLibrary.Constants;

namespace ZLibrary.Managers.Objects
{
    public class InfoManager
    {
        public static Dictionary<uint, string> Names;

        public static void Boot()
        {
            Names = new Dictionary<uint, string>();

            uint infoOffsetPtr = ZCore.BlackMagic.ReadUInt(ZCore.MainInfoOffsetPtr + 0x24);
            uint infoCount = ZCore.BlackMagic.ReadUInt(infoOffsetPtr + 0x1c);

            uint infoPtr = ZCore.BlackMagic.ReadUInt(ZCore.BlackMagic.ReadUInt(ZConstants.BASE_INFO_95883 + 0x24) + 0x18);

            for (int i = 0; i < infoCount; i++)
            {
                uint currentInfoObj = infoPtr + (uint)(i * 0xe0);
                uint currentObjNamePtr = ZCore.BlackMagic.ReadUInt(currentInfoObj + 0x70);

                string name = ZCore.BlackMagic.ReadASCIIString(currentObjNamePtr + 0x8, 20);

                uint currentObjIDPtr = ZCore.BlackMagic.ReadUInt(currentInfoObj + 0x04);

                Names.Add(currentObjIDPtr, name);
                // add stuff
            }
        }

        public static void Update()
        {
            
        }
        /*

       
            Console.WriteLine("Players: " + infoCount);
            Console.Read();
    }
         * */
    }
}
