﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZLibrary.Constants
{
    public class ZConstants
    {
        // [Constants]
        public const string APPLICATION = "arma2oa";

        // [Base Addresses]
        public const uint BASE_95208 = 0xDD3D48;
        public const uint BASE_95248 = 0xDE20A8;
        public const uint BASE_95417 = 0xDE20A8;
        public const uint BASE_95660 = 0xDE0FF8;
        public const uint BASE_95724 = 0xDD3D48;
        public const uint BASE_95777 = 0xDD3D48;

        public const uint BASE_95883 = 0xDE1FF8; // Find by ollydbg -> view arma2oa module -> view name in all modules -> CDPCreateclient -> first mov eax
        public const uint BASE_INFO_95883 = 0xDD3CA8; // find by search all text strings -> search for "password" -> find password in text strings which is 1 above maxPlayers -> count 31 spaces up -> should be "mov ecx arma2.xxxx"

        public const uint BASE_96061 = 0xDDDFE0;
        public const uint BASE_INFO_96061 = 0xDCFC88;

        public static string[] VEHICLE_MODELS = new string[21] {
            "UH1H_DZ",
            "car_hatchback",
            "Tractor",
            "TT650_Ins",
            "TT650_Civ",
            "Old_bike_TK_CIV_EP1",
            "Old_bike_TK_INS_EP1",
            "Ikarus",
            "V3S_Civ",
            "Ural_TK_CIV_EP1",
            "Lada2",
            "UAZ_CDF",
            "Volha_2_TK_CIV_EP1",
            "hilux_civil_3_open",
            "hilux1_civil_3_open",
            "S1203_TK_CIV_EP1",
            "Smallboat_1",
            "smallboat_2",
            "PBX",
            "ATV_CZ_EP1",
            "ATV_US_EP1"
        };

        public static string[] PLAYER_MODELS = new string[9] {
            "Survivor1_DZ",
            "Survivor2_DZ",
            "SurvivorW2_DZ",
            "Sniper1_DZ",
            "Camo1_DZ",
            "Bandit1_DZ",
            "Hero1_DZ",
            "Soldier1_DZ",
            "Rocket_DZ"
        };

        public static string[] WANTED_ITEMS = new string[2] {
            "TentStorage",
            "UH1Wreck"
        };
        
    }

    public enum EntityType
    {
        Vehicle,
        Player,
        Other, // Tentz n Shit
        Unknown
    }
}
